package com.mysite.webservice4java.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mysite.webservice4java.client.service.WeatherService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class })
public class CxfWebserviceTest {

	@Autowired
	private WeatherService weatherService;

	@Test
	public void getSupportProvince() throws Exception {
		weatherService.getSupportProvince();
	}

	@Test
	public void getSupportCity() throws Exception {
		weatherService.getSupportCity("湖南");
	}

	@Test
	public void getWeatherbyCityName() throws Exception {
		// 该方法调用报错，原因未知
		weatherService.getWeatherbyCityName("长沙");
	}
}
