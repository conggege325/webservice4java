package com.mysite.webservice4java.client;

import org.apache.axis2.AxisFault;
import org.junit.Before;
import org.junit.Test;

import com.mysite.webservice4java.client.generate.axis2.WeatherWebServiceStub;
import com.mysite.webservice4java.client.generate.axis2.WeatherWebServiceStub.ArrayOfString;

public class Axis2WebserviceTest {

	public WeatherWebServiceStub stub;

	@Before
	public void init() throws Exception {
		stub = new WeatherWebServiceStub();
	}

	@Test
	public void getSupportProvince() throws Exception {
		WeatherWebServiceStub.GetSupportProvince getSupportProvince = new WeatherWebServiceStub.GetSupportProvince();
		WeatherWebServiceStub.GetSupportProvinceResponse getSupportProvinceResponse = stub.getSupportProvince(getSupportProvince);
		ArrayOfString arrayOfString = getSupportProvinceResponse.getGetSupportProvinceResult();
		String[] array = arrayOfString.getString();
		for (String string : array) {
			System.out.println(string);
		}
	}

	@Test
	public void getSupportCity() throws Exception {
		WeatherWebServiceStub.GetSupportCity getSupportCity = new WeatherWebServiceStub.GetSupportCity();
		getSupportCity.setByProvinceName("湖南");
		WeatherWebServiceStub.GetSupportCityResponse response = stub.getSupportCity(getSupportCity);
		ArrayOfString arrayOfString = response.getGetSupportCityResult();
		String[] array = arrayOfString.getString();
		for (String string : array) {
			System.out.println(string);
		}
	}

	@Test
	public void getWeatherbyCityName() throws Exception {
		WeatherWebServiceStub.GetWeatherbyCityName getWeatherbyCityName = new WeatherWebServiceStub.GetWeatherbyCityName();
		getWeatherbyCityName.setTheCityName("长沙");
		WeatherWebServiceStub.GetWeatherbyCityNameResponse getWeatherbyCityNameResponse = stub.getWeatherbyCityName(getWeatherbyCityName);
		ArrayOfString arrayOfString1 = getWeatherbyCityNameResponse.getGetWeatherbyCityNameResult();
		String[] array1 = arrayOfString1.getString();
		for (String string : array1) {
			System.out.println(string);
		}
	}
}
