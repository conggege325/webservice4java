package com.mysite.webservice4java.client.config;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.mysite.webservice4java.client.generate.cxf.WeatherWebServiceSoap;

@Configuration
@Lazy
public class WebserviceConfig {

	@Bean
	public JaxWsProxyFactoryBean jaxWsProxyFactoryBean() {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl");
		factory.setServiceClass(WeatherWebServiceSoap.class);
		return factory;
	}

	@Bean
	public WeatherWebServiceSoap weatherWebservice(JaxWsProxyFactoryBean factory) {
		return (WeatherWebServiceSoap) factory.create();
	}
}
