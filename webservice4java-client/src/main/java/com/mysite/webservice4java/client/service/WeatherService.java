package com.mysite.webservice4java.client.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysite.webservice4java.client.generate.cxf.ArrayOfString;
import com.mysite.webservice4java.client.generate.cxf.WeatherWebServiceSoap;

@Service
public class WeatherService {

	@Autowired
	private WeatherWebServiceSoap weatherWebServiceSoap;

	public void getSupportProvince() {
		ArrayOfString arrayOfString = weatherWebServiceSoap.getSupportProvince();
		System.out.println(arrayOfString);
		List<String> strList = arrayOfString.getString();
		for (String str : strList) {
			System.out.println(str);
		}
	}

	public void getSupportCity(String byProvinceName) {
		ArrayOfString arrayOfString = weatherWebServiceSoap.getSupportCity(byProvinceName);
		System.out.println(arrayOfString);
		List<String> strList = arrayOfString.getString();
		for (String str : strList) {
			System.out.println(str);
		}
	}
	
	public void getWeatherbyCityName(String theCityName) {
		ArrayOfString arrayOfString = weatherWebServiceSoap.getWeatherbyCityName(theCityName);
		System.out.println(arrayOfString);
		List<String> strList = arrayOfString.getString();
		for (String str : strList) {
			System.out.println(str);
		}
	}
}
