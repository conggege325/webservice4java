# webservice4java-client

#### apache cxf 说明
1. 在如下网址中下载wsdl到本地：
http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl
把文件中的 <s:element ref="s:schema" /> 全部删除

2. 利用cxf的命令行工具生成webservice的客户端代码：
wsdl2java -encoding utf-8 -p com.mysite.webservice4java.client.generate.cxf -d D:/dev/workspace/webservice4java/webservice4java-client/src/main/java -client D:/dev/workspace/webservice4java/webservice4java-client/src/main/resources/WeatherWebService.wsdl
其中：
    -encoding 为指定生成文件的字符编码
    -d 为生成目录
    -p 为生成java文件的包名
    -client 为wsdl文件的url或者本地wsdl的路径

#### apache axis2 说明
1. 利用axis2的命令行工具生成webservice的客户端代码：
wsdl2java -uri http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl -p com.mysite.webservice4java.client.generate.axis2 -o D:/dev/workspace/webservice4java/webservice4java-client/src/main/java
详细说明：
命令行格式为：WSDL2Java [options] -uri <A url or path to a WSDL>
其中常用的options具体如下：
-o <path> : 指定生成代码的输出路径
-a : 生成异步模式的代码
-s : 生成同步模式的代码
-p <pkg> : 指定代码的package名称
-l <languange> : 使用的语言(Java/C) 默认是java
-t : 为代码生成测试用例
-ss : 生成服务端代码；默认不生成
-sd : 生成服务描述文件 services.xml,仅与-ss一同使用
-d <databinding> : 指定databingding，例如，adb、xmlbeans、jibx、jaxme、 jaxbri和none（无数据绑定），默认为adb
-g : 生成服务端和客户端的代码
-pn <port_name> : 当WSDL中有多个port时，指定其中一个port
-sn <serv_name> : 选择WSDL中的一个service
-u : 展开data-binding的类
-r <path> : 为代码生成指定一个repository
-ssi : 为服务端实现代码生成接口类
-S : 为生成的源码指定存储路径
-R : 为生成的resources指定存储路径
–noBuildXML : 输出中不生成build.xml文件
–noWSDL : 在resources目录中不生成WSDL文件
–noMessageReceiver : 不生成MessageReceiver类